/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
require( './styles/front/global.scss');

// start the Stimulus application
import './bootstrap';

import 'bootstrap/scss/bootstrap.scss';

import '@fortawesome/fontawesome-free/scss/solid.scss'

/*import $ from 'jquery';

$(document).ready(function() {

});*/


import './styles/app.scss';
import '@popperjs/core';
import 'bootstrap';
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');
require('bootstrap-datepicker/dist/js/bootstrap-datepicker');
require('bootstrap-datepicker/js/locales/bootstrap-datepicker.fr');
require('bootstrap-datepicker/dist/css/bootstrap-datepicker.css');
require('bootstrap')
const jQuery = $ = require('jquery');
global.$ = global.jQuery = $;
import moment from 'moment';

moment.locale("fr_FR");



$(document).ready(function () {

    let position_one=$('#one').offset().top;
    let position_two=$('#two').offset().top;
    let position_tree=$('#tree').offset().top;
    let position_four=$('#four').offset().top;
    let position_five=$('#five').offset().top;
    let position_six=$('#six').offset().top;

    if($("#skills").length>0){
        $.getJSON('/api/skills',skills=>{
            let compt =1;
            skills.forEach(skills=>{
                $("#skills").append(
                    `<div class="col-5 skill-container boxload">
                        <div>${skills.name}</div>
                         <div class="progress mx-auto" data-value='${skills.level}'>
                          <span class="progress-left">
                              <span class="progress-bar border-primary"></span>
                          </span>
                          <span class="progress-right">
                              <span class="progress-bar border-primary"></span>
                          </span>
                          <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                            <div class="h4 font-weight-bold">${skills.level*10}<sup class="h6">%</sup></div>
                          </div>
                    </div>
                </div>`
                );
               /* let container =  $(".skill-container");
                if(compt%2===0)
                {
                    container.addClass("skill-left-container");
                }else{
                    container.addClass("skill-right-container");
                }

                if(compt!==1 &&compt!==2)
                {
                   container.addClass("skill-container-top-border");
                }*/


                $(function() {

                    $(".progress").each(function () {

                        let value = $(this).attr('data-value')*10;
                        let left = $(this).find('.progress-left .progress-bar');
                        let right = $(this).find('.progress-right .progress-bar');
                        if (value > 0) {
                            if (value <= 50) {
                                right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
                            } else {
                                right.css('transform', 'rotate(180deg)')
                                left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
                            }
                        }

                    })

                    function percentageToDegrees(percentage) {

                        return percentage / 100 * 360

                    }

                })
               /* console.log(container.data('value'))
                compt++;*/

            })
        })
    }
    if($("#references").length>0){
        $.getJSON('/api/references',references=>{
            references.forEach(references=>{
                $("#references").append(
                    `<div class="box boxload"  ">
                        <div class="img-box">
                            <img src="uploads/images/${references.mediaFileName}" alt="">
                            <div class="over-frame" id="${references.id}">
                                <div class="infos-ref">${references.description}</div>
                            </div>
                        </div>
                        <div class="text-box" data-id="${references.id}">
                            <div>${references.name}</div>
                            <div>${references.company}</div>
                        </div>
                    </div>`

                );
            })
            $('.text-box').mouseover(function(){
               let idBox=$(this).data('id');
               $("#"+idBox).css({"-webkit-transform":"translate(0,0)"});
               //console.log($("#13"));
            });
            $('.text-box').mouseout(function(){
                let idBox=$(this).data('id');
                $("#"+idBox).css({"-webkit-transform":"translate(0,95px)"});
                //console.log($("#13"));
            });

            $('.contact').mouseover(function(){
                let id =$(this).data('id');
                //console.log($('#gt-'+id));
                $('#gt-'+id).css({"-webkit-transform":"translate(0,-2px)"})
                $('#gb-'+id).css({"-webkit-transform":"translate(0,-15px)"})
            });
            $('.contact').mouseout(function(){
                let id =$(this).data('id');
                //console.log($('#gt-'+id));
                $('#gt-'+id).css({"-webkit-transform":"translate(0,10px)"})
                $('#gb-'+id).css({"-webkit-transform":"translate(0,-30px)"})
            })

        })
    }

    if($("#formations").length>0){
        $.getJSON('/api/formations',formations=>{
            formations.forEach(formations=>{
                $("#formations").append(
                    `<div class="entry boxload">
                        <div class="title">
                          <h3>${moment(formations.startedAt).format('MMMM-YYYY')}  ${formations.name}</h3>
                          <p>${formations.school}</p>
                        </div>
                        <div class="body-tl">
                          <p>
                          ${formations.description}
                        </p>
                        </div>
                      </div>
                    </div>`

                );
            })
        })
    }

    if($("#experiences").length>0){
        $.getJSON('/api/experiences',experiences=>{
            experiences.forEach(experiences=>{
                $("#experiences").append(
                    `<div class="entry boxload">
                        <div class="title">
                          <h3>${moment(experiences.startedAt).format('MMMM-YYYY')}  ${experiences.name}</h3>
                          <p>${experiences.company}</p>
                        </div>
                        <div class="body-tl">
                          <p>
                          ${experiences.description}
                        </p>
                        </div>
                      </div>
                    </div>`

                );
            })
        })
    }

    $(window).scroll(function(){
        if(position_one===position_one && position_two===position_two && position_tree===position_tree && position_four===position_four && position_five===position_five && position_six===position_six)
            position_one=$('#one').offset().top;
            position_two=$('#two').offset().top;
            position_tree=$('#tree').offset().top;
            position_four=$('#four').offset().top;
            position_five=$('#five').offset().top;
            position_six=$('#six').offset().top;

        $('.header').css("opacity", 1- $(window).scrollTop() / 500);
        if( window.matchMedia("(min-width: 576px)").matches) {
            if ($(window).scrollTop() > position_six - 200) {
                $('.body').css({"background-image": "url('build/images/sunset2.jpg')"});
            } else if ($(window).scrollTop() > position_five - 100) {
                $('.body').css({"background-image": "url('build/images/irish_bus.jpg')"});
            } else if ($(window).scrollTop() > position_four - 100) {
                $('.body').css({"background-image": "url('build/images/derviche.jpg')"});
            } else if ($(window).scrollTop() > position_tree - 100) {
                $('.body').css({"background-image": "url('build/images/got.jpg')"});
            } else if ($(window).scrollTop() > position_two - 100) {
                $('.body').css({"background-image": "url('build/images/pala.jpg')"});
            } else if ($(window).scrollTop() > 20) {
                $('.body').css({"background-image": "url('build/images/picmidi.jpg')"});
            } else if ($(window).scrollTop() <= 0) {
                $('.body').css({"background-image": "url('build/images/sunset1.jpg')"});
            }
        }
    });

    $(".goto-next").click(function() {
        let id =$(this).data('id');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(id).offset().top
        },
        1000,"linear");
    });



});






