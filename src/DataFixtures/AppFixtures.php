<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private  $userPasswordEncoder;

    /**
     * @param UserPasswordHasherInterface $userPasswordEncoder
     */
    public function  __construct(UserPasswordHasherInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }


    public function load(ObjectManager $manager): void
    {
        $admin =new User();
        $admin->setPassword($this->userPasswordEncoder->hashPassword($admin, ''));
        $admin->setEmail('andre.szetela@gmail.com');
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);


        $manager->flush();
    }
}
