<?php

namespace App\Form;

use App\Entity\References;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class ReferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add("name", TextType::class, [
                "label"=> "Intitulé de l'expérience",
                'required'=>true,
                'attr'=>[
                    'placeholder'=>'Intitulé de l\'expérience'
                ]
            ])

            ->add("description", TextareaType::class, [
                "label"=> "Description de la référence",
                'attr'=>[
                    'placeholder'=>'Description de la référence'
                ]
            ])

            ->add("company", TextType::class, [
                "label"=> "Nom de l'entreprise",
                'required'=>true,
                'attr'=>[
                    'placeholder'=>'Nom de l\'entreprise'
                ]
            ])

            ->add("startedAt", DateType::class, [
                "label"=> "Début",
                'required' => true,
                'widget' => 'single_text'
            ])

            ->add("endedAt", DateType::class, [
                "label"=> "Fin",
                'widget' => 'single_text',
            ])

            ->add("media", FileType::class,[
                "label"=>"Image de la référence",
                'mapped'=>false,
                'required'=>false,
                'constraints'=>new Image()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver); // TODO: Change the autogenerated stub

        $resolver->setDefault("data_class", References::class);

    }

}