<?php

namespace App\Controller\API;

use App\Repository\ReferencesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ReferenceController
 * @package App\Controller\API
 * @Route ("/api/references")
 */
class ReferenceController extends AbstractController
{
    /**
     * @Route ("",methods={"GET"}, name="api_references_collection_get")
     * @param ReferencesRepository $referenceRepository
     * @return Response
     */
    public function collection(SerializerInterface $serializer,ReferencesRepository $referenceRepository): Response
    {
        $references_data= $referenceRepository->findBy(['isActive'=>true], ['startedAt'=>'DESC']);
        $references_data=$serializer->serialize($references_data, 'json');
        //return $this->json($references_data);
        return new JsonResponse(json_decode($references_data));
    }
}