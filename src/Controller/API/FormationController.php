<?php

namespace App\Controller\API;

use App\Repository\FormationsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class FormationController
 * @package App\Controller\API
 * @Route ("/api/formations")
 */
class FormationController extends AbstractController
{
    /**
     * @Route ("",methods={"GET"}, name="api_formations_collection_get")
     * @param FormationsRepository $formationRepository
     * @return Response
     */
    public function collection(SerializerInterface $serializer,FormationsRepository $formationRepository): Response
    {
        $formations_data= $formationRepository->findBy(['isActive'=>true], ['startedAt'=>'DESC']);
        $formations_data=$serializer->serialize($formations_data, 'json');
        //return $this->json($formations_data);
        return new JsonResponse(json_decode($formations_data));
    }
}