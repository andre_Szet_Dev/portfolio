<?php

namespace App\Controller\API;

use App\Repository\ExperiencesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ExperienceController
 * @package App\Controller\API
 * @Route ("/api/experiences")
 */
class ExperienceController extends AbstractController
{
    /**
     * @Route ("",methods={"GET"}, name="api_experiences_collection_get")
     * @param ExperiencesRepository $experienceRepository
     * @return Response
     */
    public function collection(SerializerInterface $serializer,ExperiencesRepository $experienceRepository): Response
    {
        $experiences_data= $experienceRepository->findBy(['isActive'=>true], ['startedAt'=>'DESC']);
        $experiences_data=$serializer->serialize($experiences_data, 'json');
        //return $this->json($experiences_data);
        return new JsonResponse(json_decode($experiences_data));
    }
}