<?php

namespace App\Controller\API;

use App\Repository\SkillsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class SkillController
 * @package App\Controller\API
 * @Route ("/api/skills")
 */
class SkillController extends AbstractController
{
    /**
     * @Route ("",methods={"GET"}, name="api_skills_collection_get")
     * @param SerializerInterface $serializer
     * @param SkillsRepository $skillRepository
     * @return Response
     */
    public function collection(SerializerInterface $serializer,SkillsRepository $skillRepository): Response
    {
        $skills_data= $skillRepository->findBy(['isActive'=>true]);
        $skills_data=$serializer->serialize($skills_data, 'json');
        //return $this->json($skills_data);
        return new JsonResponse(json_decode($skills_data));
    }
}