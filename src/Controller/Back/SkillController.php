<?php

namespace App\Controller\Back;

use App\Entity\Skills;
use App\Form\SkillType;
use App\Repository\SkillsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class SkillController
 * @package App\Controller\Back\
 * @Route("/back/skills")
 */

class SkillController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/", name="skills_manage")
     * @param SkillsRepository $skillRepository
     * @return Response
     */
    public function manage(SkillsRepository $skillRepository): Response
    {
        $skills = $skillRepository->findBy(["isActive" =>true]);
        return $this->render("back/skill/manage.html.twig",[
            "skills"=> $skills
        ]);
    }

    /**
     * @Route("/create", name="skill_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $skill = new Skills();
        $form = $this->createForm(SkillType::class, $skill)->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $this->doctrine->getManager()->persist($skill);
            $this->doctrine->getManager()->flush();
            $this->addFlash("success","La compétence a été ajouté avec succès");

            return $this->redirectToRoute("skills_manage");
        }

        return $this->render("back/skill/create.html.twig",[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/edit/skill/{id}", name="edit_skill")
     * @param Request $request
     * @param Skills $skill
     * @return Response
     */
    public function update(Request $request, Skills $skill): Response{
        $form = $this->createForm(SkillType::class, $skill);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $skill->setUpdatedAt(new \DateTime('now'));
            $this->doctrine->getManager()->persist($skill);
            $this->doctrine->getManager()->flush();

            return $this->redirectToRoute("skills_manage");
        }
        return $this->render("back/skill/edit.html.twig",[
            "form"=>$form->createView()
        ]);

    }

    /**
     * @Route ("/delete/skill/{id}", name="delete_skill")
     * @param Skills $skill
     * @return Response
     */
    public function delete(Skills $skill):Response
    {
        $this->doctrine->getManager();
        $skill->setIsActive(!$skill->getIsActive());
        $skill->setDeletedAt(new \DateTime('now'));
        $this->doctrine->getManager()->persist($skill);
        $this->doctrine->getmanager()->flush();

        return $this->redirectToRoute("skills_manage");
    }


}