<?php

namespace App\Controller\Back;

use App\Entity\References;
use App\Form\ReferenceType;
use App\Repository\ReferencesRepository;
use App\Services\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class ReferenceController
 * @package App\Controller\Back\
 * @Route("/back/references")
 */

class ReferenceController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/", name="references_manage")
     * @param ReferencesRepository $referencesRepository
     * @return Response
     */
    public function manage(ReferencesRepository $referencesRepository): Response
    {
        $reference = $referencesRepository->findBy(["isActive" =>true]);
        return $this->render("back/reference/manage.html.twig",[
            "references"=> $reference
        ]);
    }

    /**
     * @Route("/create", name="reference_create")
     * @param Request $request
     * @param FileUploader $fileUploader
     * @return Response
     */
    public function create(Request $request, FileUploader $fileUploader): Response
    {
        $reference = new References();
        $form = $this->createForm(ReferenceType::class, $reference)->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            /** @var UploadedFile $mediaFile */
            $mediaFile = $form->get('media')->getData();
            if ($mediaFile) {
                $brochureFileName = $fileUploader->upload($mediaFile);
                $reference->setMediaFileName($brochureFileName);
            }

            $this->doctrine->getManager()->persist($reference);
            $this->doctrine->getManager()->flush();
            $this->addFlash("success","L'expérience a été ajouté avec succès");

            return $this->redirectToRoute("references_manage");
        }

        return $this->render("back/reference/create.html.twig",[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/edit/reference/{id}", name="edit_reference")
     * @param Request $request
     * @param References $reference
     * @return Response
     */
    public function update(Request $request, References $reference): Response{
        $form = $this->createForm(ReferenceType::class, $reference);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $reference->setUpdatedAt(new \DateTime('now'));
            $this->doctrine->getManager()->persist($reference);
            $this->doctrine->getManager()->flush();

            return $this->redirectToRoute("references_manage");
        }
        return $this->render("back/reference/edit.html.twig",[
            "form"=>$form->createView()
        ]);

    }

    /**
     * @Route ("/delete/reference/{id}", name="delete_reference")
     * @param References $reference
     * @return Response
     */
    public function delete(References $reference):Response
    {
        $this->doctrine->getManager();
        $reference->setIsActive(!$reference->getIsActive());
        $reference->setDeletedAt(new \DateTime('now'));
        $this->doctrine->getManager()->persist($reference);
        $this->doctrine->getmanager()->flush();

        return $this->redirectToRoute("references_manage");
    }

    /**
     * @Route("/display/reference/{id}", name="display_reference")
     * @param References $reference
     * @return Response
     */
    public function display(References $reference):Response
    {
        $param_render=[
            "reference"=>$reference
        ];
        return $this->render("Back/reference/_ajax/view.html.twig", $param_render);
    }


}