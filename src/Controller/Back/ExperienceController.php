<?php

namespace App\Controller\Back;

use App\Entity\Experiences;
use App\Form\ExperienceType;
use App\Repository\ExperiencesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ExperienceController
 * @package App\Controller\Back\
 * @Route("/back/experiences")
 */

class ExperienceController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/", name="experiences_manage")
     * @param ExperiencesRepository $experiencesRepository
     * @return Response
     */
    public function manage(ExperiencesRepository $experiencesRepository): Response
    {
        $experience = $experiencesRepository->findBy(["isActive" =>true]);
        return $this->render("back/experience/manage.html.twig",[
            "experiences"=> $experience
        ]);
    }

    /**
     * @Route("/create", name="experience_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $experience = new Experiences();
        $form = $this->createForm(ExperienceType::class, $experience)->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $this->doctrine->getManager()->persist($experience);
            $this->doctrine->getManager()->flush();
            $this->addFlash("success","L'expérience a été ajouté avec succès");

            return $this->redirectToRoute("experiences_manage");
        }

        return $this->render("back/experience/create.html.twig",[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/edit/experience/{id}", name="edit_experience")
     * @param Request $request
     * @param Experiences $experience
     * @return Response
     */
    public function update(Request $request, Experiences $experience): Response{
        $form = $this->createForm(ExperienceType::class, $experience);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $experience->setUpdatedAt(new \DateTime('now'));
            $this->doctrine->getManager()->persist($experience);
            $this->doctrine->getManager()->flush();

            return $this->redirectToRoute("experiences_manage");
        }
        return $this->render("back/experience/edit.html.twig",[
            "form"=>$form->createView()
        ]);

    }

    /**
     * @Route ("/delete/experience/{id}", name="delete_experience")
     * @param Experiences $experience
     * @return Response
     */
    public function delete(Experiences $experience):Response
    {
        $this->doctrine->getManager();
        $experience->setIsActive(!$experience->getIsActive());
        $experience->setDeletedAt(new \DateTime('now'));
        $this->doctrine->getManager()->persist($experience);
        $this->doctrine->getmanager()->flush();

        return $this->redirectToRoute("experiences_manage");
    }

    /**
     * @Route("/display/experience/{id}", name="display_experience")
     * @param Experiences $experience
     * @return Response
     */
    public function display(Experiences $experience):Response
    {
        $param_render=[
            "experience"=>$experience
        ];
        return $this->render("Back/experience/_ajax/view.html.twig", $param_render);
    }


}