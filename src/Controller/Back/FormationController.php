<?php

namespace App\Controller\Back;

use App\Entity\Formations;
use App\Form\FormationType;
use App\Repository\FormationsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class FormationController
 * @package App\Controller\Back\
 * @Route("/back/formations")
 */

class FormationController extends AbstractController
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @Route("/", name="formations_manage")
     * @param FormationsRepository $formationsRepository
     * @return Response
     */
    public function manage(FormationsRepository $formationsRepository): Response
    {
        $formation = $formationsRepository->findBy(["isActive" =>true]);
        return $this->render("back/formation/manage.html.twig",[
            "formations"=> $formation
        ]);
    }

    /**
     * @Route("/create", name="formation_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $formation = new Formations();
        $form = $this->createForm(FormationType::class, $formation)->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $this->doctrine->getManager()->persist($formation);
            $this->doctrine->getManager()->flush();
            $this->addFlash("success","La compétence a été ajouté avec succès");

            return $this->redirectToRoute("formations_manage");
        }

        return $this->render("back/formation/create.html.twig",[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/edit/formation/{id}", name="edit_formation")
     * @param Request $request
     * @param Formations $formation
     * @return Response
     */
    public function update(Request $request, Formations $formation): Response{
        $form = $this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
        {
            $formation->setUpdatedAt(new \DateTime('now'));
            $this->doctrine->getManager()->persist($formation);
            $this->doctrine->getManager()->flush();

            return $this->redirectToRoute("formations_manage");
        }
        return $this->render("back/formation/edit.html.twig",[
            "form"=>$form->createView()
        ]);

    }

    /**
     * @Route ("/delete/formation/{id}", name="delete_formation")
     * @param Formations $formation
     * @return Response
     */
    public function delete(Formations $formation):Response
    {
        $this->doctrine->getManager();
        $formation->setIsActive(!$formation->getIsActive());
        $formation->setDeletedAt(new \DateTime('now'));
        $this->doctrine->getManager()->persist($formation);
        $this->doctrine->getmanager()->flush();

        return $this->redirectToRoute("formations_manage");
    }

    /**
     * @Route("/display/formation/{id}", name="display_formation")
     * @param Formations $formation
     * @return Response
     */
    public function display(Formations $formation):Response
    {
        $param_render=[
            "formation"=>$formation
        ];
        return $this->render("Back/formation/_ajax/view.html.twig", $param_render);
    }


}