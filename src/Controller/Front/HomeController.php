<?php

namespace App\Controller\Front;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller\Front
 * @Route("/", name="home")
 */
class HomeController extends AbstractController
{
    public function __invoke():Response
    {

        return $this->render("front/home.html.twig");
    }
}